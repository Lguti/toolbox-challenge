const express = require("express");
const { downloadAndFormatterFiles, getFiles } = require("./utils")
const router = express.Router();

// @route       GET api/v1/files/data
// @desc        Get all Files ready to be consume in the front
// @access      private

router.get("/files/data", async (req, res) => {
    try {
        const formatteredFiles = await downloadAndFormatterFiles();
        res.json({ data: formatteredFiles })
    } catch (err) {
        console.error(`Error in /api/v1/files/data: ${err.message}`)
        res.status(500).send({ message: err.message })
    }
})

// @route       GET api/v1/files/list
// @desc        Get all Files of the external API
// @access      private

router.get("/files/list", async (req, res) => {
    try {
        const files = await getFiles();
        res.json({ data: files });
    } catch (err) {
        console.error(`Error in /api/v1/files/list: ${err.message}`)
        res.status(500).send({ message: err.message })
    }
})

module.exports = router;