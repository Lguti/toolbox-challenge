const baseUrl = "https://echo-serv.tbxnet.com/v1";
const fetch = require("node-fetch");
const headers = { "Authorization": "Bearer aSuperSecretKey" };
const csv = require("csv-parser");


/**
 * @returns Promise with files name
 */
const fetchFiles = async () => {
    const filesRes = await fetch(`${baseUrl}/secret/files`, { headers });
    return filesRes;
}

/**
 * @param {req.body} stream 
 * @returns Promise resolve with validated and formatted file object or null
 */
const parserCsvFile = (stream) => {
    const parser = stream.pipe(csv());
    let fileFormat = {};
    parser.on("data", (data) => {
        if (
            Object.keys(data).length === 4 &&
            Object.values(data).filter((value) => value !== "").length === 4
        ) {
            fileFormat = {
                ...fileFormat,
                file: data.file,
                lines: [
                    ...(fileFormat.lines || []),
                    {
                        text: data.text,
                        number: data.number,
                        hex: data.hex,
                    },
                ],
            };
        }
    });

    return new Promise((resolve) => {
        parser.on("finish", () => {
            if (Object.keys(fileFormat).length !== 0) {
                resolve(fileFormat);
            } else {
                resolve(null);
            }
        });
    });
}

/**
 * @param {string[]} files - An array of file names.
 * @returns An Array with all the file promises resolve with their file formatted object or null
 */
const formatterFiles = async (files) => {
    return await Promise.all(
        files.map(async (file) => {
            try {
                const res = await fetch(`${baseUrl}/secret/file/${file}`, { headers });
                if (!res.ok) {
                    throw new Error(`Failed to fetch file ${file}: ${res.status} ${res.statusText}`);
                }
                const data = await parserCsvFile(res.body);
                return data;

            } catch (err) {
                console.error(`Error fetching file ${file}: ${err.message}`);
                return null;
            }
        })
    );
}

/**
 * @returns An array of files names
 */
const getFiles = async () => {
    try {
        const filesRes = await fetchFiles();
        if (!filesRes.ok) {
            throw new Error(`Request failed with status ${filesRes.status}`);
        }
        const { files } = await filesRes.json();

        return files;
    } catch (err) {
        console.error(`Error in getFiles: ${err.message}`)
        throw err;
    }
}

/**
 * @returns Validated and formatted array of files
 */
const downloadAndFormatterFiles = async () => {
    try {
        const files = await getFiles();
        const formattedFiles = await formatterFiles(files)

        return formattedFiles.filter((item) => item !== null);
    } catch (err) {
        console.log(`Error in downloadAndFormatterFiles: ${err.message}`);
        throw err;
    }
}

module.exports = {
    downloadAndFormatterFiles, baseUrl, headers, getFiles
}