const chai = require("chai");
const chaiHttp = require("chai-http");
const { baseUrl, headers, downloadAndFormatterFiles } = require("../utils");


const expect = chai.expect;
chai.use(chaiHttp);

describe("All test in utils.js", () => {
    it("Should return the files response", async () => {
        const res = await chai.request(baseUrl)
            .get("/secret/files")
            .set(headers);

        expect(res).to.have.status(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an("object");
        expect(res.body.files).to.be.an("array");
    });

    it("Should return 401", async () => {
        const res = await chai.request(baseUrl)
            .get("/secret/files")

        expect(res).to.have.status(401);
        expect(res.body).to.be.an("object");
        expect(res.body).to.haveOwnProperty("message");
    });

    it("should return an array of objects", async () => {
        const formattedFiles = await downloadAndFormatterFiles();
        expect(formattedFiles).to.be.an("array");
        expect(formattedFiles.every(item => typeof item === "object")).to.be.true;
    });

});