const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../../server");

chai.use(chaiHttp);
const expect = chai.expect;

describe("GET /api/v1", () => {
    it("should return a 200 status code in /files/data", (done) => {
        chai.request(app)
            .get("/api/v1/files/data")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an("object").and.haveOwnProperty("data");
                expect(res.body.data.length).to.have.at.least(1);
                done();
            });
    });
    it("should return a 404 status code in /files/data", (done) => {
        chai.request(app)
            .get("/api/wrong")
            .end((err, res) => {
                expect(res).to.have.status(404);
                done();
            });
    });
    it("should return a 200 status code in /files/list", (done) => {
        chai.request(app)
            .get("/api/v1/files/list")
            .end((err, res) => {
                expect(res).to.have.status(200);
                expect(res.body).to.be.an("object").and.haveOwnProperty("data");
                expect(res.body.data).to.be.an('array');
                expect(res.body.data.every(item => typeof item === "string")).to.be.true;
                done();
            });
    });
    it("should return a 404 status code in /files/list", (done) => {
        chai.request(app)
            .get("/api/wrong")
            .end((err, res) => {
                expect(res).to.have.status(404);
                done();
            });
    });
});