import { legacy_createStore as createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import rootReducer from "./reducers";

const INI_STATE = {}

const middleware = [thunk]

const store = createStore(
    rootReducer,
    INI_STATE,
    composeWithDevTools(applyMiddleware(...middleware))
)

export default store