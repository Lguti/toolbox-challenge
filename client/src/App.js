import React, { useEffect } from "react";
import FileTable from "./components/FileTable";
import "./App.css";

// Redux
import { Provider } from "react-redux";
import store from "./store";
import { getFilesData } from "./actions/files";
const App = () => {

  useEffect(() => {
    store.dispatch(getFilesData())
  }, [])

  return (
    <Provider store={store}>
      <FileTable />
    </Provider>
  )
}

export default App;
