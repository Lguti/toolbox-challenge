import axios from 'axios'
import {
    GET_FILES_DATA,
    FILES_DATA_ERROR,
} from './types'

// Get files data
export const getFilesData = () => async dispatch => {
    try {
        const res = await axios.get('/api/v1/files/data')

        dispatch({
            type: GET_FILES_DATA,
            payload: res.data
        })
    } catch (err) {
        dispatch({
            type: FILES_DATA_ERROR,
            payload: { msg: err.message }
        })
    }
}