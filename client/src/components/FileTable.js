import React from "react";
import { connect } from "react-redux";
import Table from "react-bootstrap/Table";
import Spinner from "./Spinner";
import FileLines from "./FileLines";
import PropTypes from "prop-types";

const FileTable = ({ loading, error, files }) => {
    return (
        <section>
            <div className="dark-overlay">
                <div className="challenge-inner">
                    <div className="content-challenge">
                        {loading && <Spinner />}
                        {!loading && (
                            <>
                                <h5 className="text-left title">{error?.msg ? `Error: ${error.msg}` : "React Test App"}</h5>
                                <Table responsive striped>
                                    <thead>
                                        <tr>
                                            <th>File name</th>
                                            <th>Text</th>
                                            <th>Number</th>
                                            <th>Hex</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            files?.data && files?.data?.map(({ file, lines }, index) =>
                                                <FileLines key={`${files}-${index}`} file={file} lines={lines} />
                                            )
                                        }
                                    </tbody>
                                </Table>
                            </>
                        )
                        }
                    </div>
                </div>
            </div>
        </section>
    )
}

FileTable.propTypes = {
    loading: PropTypes.bool,
    error: PropTypes.shape({ msg: PropTypes.string }),
}

const mapStateToProps = state => ({
    files: state.files.files,
    loading: state.files.loading,
    error: state.files.error
})

export default connect(mapStateToProps)(FileTable)