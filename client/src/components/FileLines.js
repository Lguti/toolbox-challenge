import React from "react";
import PropTypes from "prop-types";

const FileLines = ({ file, lines }) => {
    const elements = lines.map((line, index) => {
        if (lines.length <= index) {
            return null
        } else {
            return (
                <tr key={`${line.text}-${index}`} role="gridcell">
                    <td>{file}</td>
                    <td>{line.text}</td>
                    <td>{line.number}</td>
                    <td>{line.hex}</td>
                </tr>
            );
        }
    });

    return elements;
};

FileLines.propTypes = {
    file: PropTypes.string.isRequired,
    lines: PropTypes.array.isRequired,
}

export default FileLines