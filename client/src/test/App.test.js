import React from "react";
import { render, screen, wait } from "@testing-library/react";
import store from "../store";
import App from "../App";

describe("App component", () => {
    it("Should call getFilesDat action on mount", () => {
        const dispatchMock = jest.fn();
        jest.spyOn(store, "dispatch").mockImplementation(dispatchMock);

        render(<App />);
        expect(dispatchMock).toHaveBeenCalledTimes(1);
    });
})