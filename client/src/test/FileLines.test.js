import React from "react";
import { render, screen } from "@testing-library/react";
import FileLines from "../components/FileLines";

const lines = [
    { text: "test1", number: "1", hex: "1994b2d579a97a3c8c5cd2e35940" },
    { text: "test2", number: "2", hex: "1994b2d579a97a3c8c5cd40" },
    { text: "test3", number: "3", hex: "1994b2d579a97af0d940" }
];

const file = "test.csv";

describe("FileLines component", () => {
    it("Should render valid file and lines", () => {
        render(<FileLines file={file} lines={lines} />);
        expect(screen.getAllByRole("gridcell")).toHaveLength(3);
    });

    it('should display correct content', () => {
        render(<FileLines file={file} lines={lines} />);
        lines.forEach(line => {
            expect(screen.getByText(line.text)).toBeDefined();
            expect(screen.getByText(line.number)).toBeDefined();
            expect(screen.getByText(line.hex)).toBeDefined();
        });
    });

})