import React from "react";
import { render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import FileTable from "../components/FileTable";

const mockStore = configureMockStore();
const store = mockStore({
    files: {
        files: {
            data: [
                {
                    file: 'file1.csv',
                    lines: [
                        { text: 'line 1', number: 1, hex: '1994b2d579a97a3c8c5cd2e0d940' },
                        { text: 'line 2', number: 2, hex: '1994b2d579a97a3d235ff0d940' },
                    ],
                },
                {
                    file: 'file2.csv',
                    lines: [
                        { text: 'line 1', number: 1, hex: '1994b2d5798c5cd2e35ff0d940' },
                        { text: 'line 2', number: 2, hex: '12d579a97a3c8c5cd2e35ff0d940' },
                    ],
                },
            ],
        },
        loading: false,
        error: {},
    },
});

const storeWithError = mockStore({
    files: {
        files: {
            data: [],
        },
        loading: false,
        error: { msg: "Something went wrong!" },
    },
});

const storeWithLoading = mockStore({
    files: {
        files: {
            data: [],
        },
        loading: true,
        error: {},
    },
});

describe("FileTable component", () => {
    it("should render correctly and render file lines when the data is correct", () => {
        render(
            <Provider store={store}>
                <FileTable />
            </Provider>
        );

        const titleElement = screen.getByText(/React Test App/);
        const fileLineElements = screen.getAllByRole("gridcell");

        expect(titleElement).toBeDefined();
        expect(fileLineElements.length).toBe(4);
    });

    it("should render error message when error prop is defined", () => {
        render(
            <Provider store={storeWithError}>
                <FileTable />
            </Provider>
        );
        const errorElement = screen.getByText(/Error: Something went wrong!/);
        expect(errorElement).toBeDefined();
    });

    it("should render loading spinner when loading prop is true", () => {
        render(
            <Provider store={storeWithLoading}>
                <FileTable />
            </Provider>
        );
        const spinnerElement = screen.getByAltText("Loading...");
        expect(spinnerElement).toBeDefined();
    });
});
