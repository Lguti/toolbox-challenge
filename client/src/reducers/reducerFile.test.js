import fileReducer from "./files";
import { GET_FILES_DATA, FILES_DATA_ERROR } from "../actions/types";

const initialState = {
    files: [],
    loading: true,
    error: {}
}

describe("files reducer", () => {
    it('should return the initial state when an unknown action is dispatched', () => {
        const unknownAction = {
            type: 'UNKNOWN_ACTION',
        };
        expect(fileReducer(initialState, unknownAction)).toEqual(initialState);
    });

    it('should handle GET_FILES_DATA action', () => {
        const action = {
            type: 'GET_FILES_DATA',
            payload: [{ file: 'file1.csv' }, { file: 'file2.csv' }],
        };

        const expectedState = {
            files: [{ file: 'file1.csv' }, { file: 'file2.csv' }],
            loading: false,
            error: {},
        };

        const result = fileReducer(initialState, action);

        expect(result).toEqual(expectedState);
    });

    it('should handle FILES_DATA_ERROR', () => {
        const error = 'Error retrieving files data';
        const errorAction = {
            type: FILES_DATA_ERROR,
            payload: error,
        };
        const expectedState = {
            files: [],
            loading: false,
            error: error,
        };
        expect(fileReducer(initialState, errorAction)).toEqual(expectedState);
    });


})