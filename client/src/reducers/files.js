import {
    GET_FILES_DATA,
    FILES_DATA_ERROR,
} from '../actions/types'

const initialState = {
    files: [],
    loading: true,
    error: {}
}

export default function (state = initialState, action) {
    const { type, payload } = action

    switch (type) {
        case GET_FILES_DATA:
            return {
                ...state,
                files: payload,
                loading: false
            }
        case FILES_DATA_ERROR:
            return {
                ...state,
                error: payload,
                files: [],
                loading: false,
            }
        default:
            return state
    }
}