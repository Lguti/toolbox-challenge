# Toolbox Challenge :rocket: 

## **Install** dependencies first, you need at least ```node version 14``` or ```node version 16``` for this project :cop: :warning:

## Root position
- #### **{{ROOT}}** -> **Is the principal folder of the project (by default toolbox-challenge)**

## Api position
- #### **{{ROOT}}/api**

## Client position
- #### **{{ROOT}}/client**

## Requirements steps
### 1 - Exec this command to install dependencies for ```api``` on the root position **/**

- ``` npm install ```

### 2- Go to ```/client``` and exec the command below to install dependencies for ```client```

- ``` npm install ```

## Next

### Test ```api``` and ```client``` exec the command below that you want on the root position **/**

- ``` npm test ``` **-> (If you want to run all ```api``` && ```client``` test)**
- ``` npm run test:api ``` **-> (If you want to run only the api test)**
- ``` npm run test:client``` **-> (If you want to run only the client test)**

### Start ```api``` and ```client``` exec the command below that you want on the root position **/**

- ``` npm start ``` **-> (If you want to start all ```api``` && ```client```)**
- ``` npm run server ``` **-> (If you want to start only the api)**
- ``` npm run client ``` **-> (If you want to start only the app)**

## Api  

- Running in **port 4000** 
- Base api url ```http://localhost:4000/api/v1``` or ```http://127.0.0.1:4000/api/v1```
- Get file data formatted ```/files/data```
### Response json of ```/files/data```
```
{
    "data": [
            {
                "file": "test1.csv",
                "lines": [
                    {
                        "text": "CqMkfmeTRMO",
                        "number": "5049",
                        "hex": "3fb81f26737edf8a4a411e08683b6f30"
                    }
                ]
            },
            {
                "file": "test2.csv",
                "lines": [
                    {
                        "text": "CqMkfmeTRMO",
                        "number": "5049",
                        "hex": "3fb81f26737edf8a4a411e08683b6f30"
                    },
                    {
                        "text": "CqMkfmeTRMO",
                        "number": "5049",
                        "hex": "3fb81f26737edf8a4a411e08683b6f30"
                    }...
                ]
            }...
        ]

}
```
- **Get list of files** ```/files/list```
### Response json of ```/files/list```
```
{
    "data": [
            "test1.csv",
            "test2.csv"...
        ]
}
```

## Client
- **Running in port 3000** 
- **Base url** ```http://localhost:3000``` or ```http://127.0.0.1:3000```
