const express = require('express');
const app = express();

// Controller
const fileRouter = require('./api/files');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));


// Routes
app.use('/api/v1', fileRouter);

const PORT = process.env.PORT || 4000;

app.listen(PORT, () => console.log(`Server started on port ${PORT}`));

module.exports = app;